#include "concurrent_queue.hxx"
#include "defines.hxx"
#include "logging/logtk/logtk.hpp"



template<typename T>
void consume(concurrent_queue<T>& q, unsigned int id)
{
}







int main(int argc, char* argv[])
{
   
    //  have to test the concurrent queue

    //  all running threads will check if the queue is empty or not.
    //  It not empty, then they will process the edge, i.e, a message will be printed by the logger.
    

    //  Initialize Logger
    logtk::start_logger("concurrent_queue_test.log",".");
    


    //  2nd Try and Locking all three threads.
    //  Try and pausing all threads
    
    logtk::stop_logger();

    return EXIT_SUCCESS;
}
