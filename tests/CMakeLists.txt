#----------------------------------------------------------------
#					aligned_allocator_test
#----------------------------------------------------------------
#add_executable(aligned_allocator_test aligned_allocator_test.cxx)
#target_link_libraries(aligned_allocator_test)
#add_test(NAME aligned_allocator_test 
		#COMMAND aligned_allocator_test)

#----------------------------------------------------------------
#					graph create test
#----------------------------------------------------------------
#add_executable(graph_create_test graph_create_test.cpp)
#target_link_libraries(graph_create_test LoggerCpp pthread ${Boost_LIBRARIES})


#----------------------------------------------------------------
#                   simple timer test
#----------------------------------------------------------------
#add_executable(simple_timer_test simple_timer_test.cxx)
#target_link_libraries(simple_timer_test)
#add_test(NAME simple_timer_test
#         COMMAND simple_timer_test)


#----------------------------------------------------------------
#                     simd test
#----------------------------------------------------------------
#add_executable(simd_test simd_test.cxx)
#target_link_libraries(simd_test)
#add_test(NAME simd_test
#        COMMAND simd_test)


#---------------------------------------------------------------
#                         cv_test
#---------------------------------------------------------------
#add_executable(cv_test cv_test.cpp)
#target_link_libraries(cv_test pthread)
#add_test(NAME cv_test
#        COMMAND cv_test)


#--------------------------------------------------------------
#                        cv_test2
#--------------------------------------------------------------
#add_executable(cv_test2 cv_test2.cpp)


#--------------------------------------------------------------
#				      concurrency_cxx
#--------------------------------------------------------------
#add_executable(concurrency_cxx concurrency_cxx.cxx)
#target_link_libraries(concurrency_cxx pthread)


#--------------------------------------------------------------
#                       cv_test3
#-------------------------------------------------------------- 
#add_executable(cv_test3 cv_test3.cxx)
#target_link_libraries(cv_test3 pthread)


#---------------------------------------------------------------
#                      handshake_MT_test
#---------------------------------------------------------------
add_executable(handshake_MT_test handshake_MT_test.cxx)
target_link_libraries(handshake_MT_test LoggerCpp pthread ${Boost_LIBRARIES})


#---------------------------------------------------------------
#                       thread_pool2
#---------------------------------------------------------------
#add_executable(thread_pool2 thread_pool2.cxx)
#target_link_libraries(thread_pool2 pthread ${Boost_LIBRARIES})


#---------------------------------------------------------------
#						LoggerCPP test
#---------------------------------------------------------------
#add_executable(logger_test logger_test.cxx )
#target_link_libraries(logger_test LoggerCpp pthread ${Boost_LIBRARIES})


#---------------------------------------------------------------
#						graph_io_test
#---------------------------------------------------------------
#add_executable(graph_io_test graph_io_test.cxx)
#target_link_libraries(graph_io_test LoggerCpp pthread ${Boost_LIBRARIES})


#---------------------------------------------------------------
#
#---------------------------------------------------------------
#add_executable(thread_pool3 thread_pool3.cxx)
#target_link_libraries(thread_pool3 LoggerCpp pthread)


#---------------------------------------------------------------
#	Second Thread Pool Test
#---------------------------------------------------------------
#add_executable(second_thread_pool_test second_thread_pool_test.cxx)
#target_link_libraries(second_thread_pool_test LoggerCpp pthread)

#---------------------------------------------------------------
#	Third Thread Pool Test
#---------------------------------------------------------------
#add_executable(third_thread_pool_test third_thread_pool_test.cxx)
#target_link_libraries(third_thread_pool_test LoggerCpp pthread)

#---------------------------------------------------------------
#
#---------------------------------------------------------------
#add_executable(threads threads.cxx)
#target_link_libraries(threads LoggerCpp pthread)

#--------------------------------------------------------------
#
#--------------------------------------------------------------
add_executable(thread_pool4 thread_pool4.cxx)
target_link_libraries(thread_pool4 LoggerCpp pthread)
