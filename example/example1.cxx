#include "handshake_algo_MT.hxx"
#include "graph_create.hxx"
#include "simple_timer.hxx"
#include "graph_io.hxx"


template<typename T>
void fn1(Graph<T>& gr)
{
    handshake_algo_MT<sse_float_4>::options opts;
    opts.num_iters=100;
    opts.num_threads=4;
    handshake_algo_MT<sse_float_4> algo(gr,opts);
    algo.infer();
//    algo.compute_primal();

//    algo.infer();   
}


template<typename T>
void fn2(Graph<T>& gr)
{
    handshake_algo_MT<sse_float_4>::options opts;
    opts.num_iters=100;
    opts.num_threads=std::thread::hardware_concurrency();
    handshake_algo_MT<sse_float_4> algo(gr,opts);
    algo.infer();   
}


int main(const int argc, const char* argv[])
{
    srand(126);

    INIT_TIMER
    int nR=3;
    int nC=3;
    int nL=3;
    int nV=4;

//    Graph<float> graph=create_grid_graph<float>(nR,nC,nL,false);
//    graph.print();
//    Graph<float> graph=create_complete_graph<float>(nV,nL,false);
//    graph.print();
	
	std::string filename=argv[1];
	Graph<float> graph=read_graph<float>(filename);


    handshake_algo_MT<sse_float_4>::options opts;
    opts.num_iters=100;
    opts.num_threads=8;
    handshake_algo_MT<sse_float_4> algo(graph,opts);
    algo.infer(); 


//    START_TIMER
//    fn1(graph);
//    STOP_TIMER("SINGLE THREAD")

//    START_TIMER
//    fn2(graph);
//    STOP_TIMER("MULTI THREAD")

    return EXIT_SUCCESS;
}
