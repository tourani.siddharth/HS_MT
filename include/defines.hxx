#pragma once

#include <chrono>
#include <string>
#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <map>
#include <mutex>
#include <thread>
#include <functional>
#include <iostream>
#include <sstream>
#include <memory>
#include <fstream>
#include <cmath>
#include <cstdio>
#include <cstdint>
#include <cstddef>
#include <cassert>
#include <algorithm>
#include <condition_variable>
#include <atomic>
#include <queue>


#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/max_cardinality_matching.hpp>

#if defined(__SSE4_2__)
    #include <smmintrin.h>
#endif

#if defined(__AVX__)
    #include <immintrin.h>
#endif

/*!
 * ************************************************************
 * ****************  Parallelization Control ******************
 * ************************************************************
 */ 

#define BFS_ROOTS 32u
#define DIV_UP(A, V) (A / V + (A % V==0 ? 0 : 1))

int RND_UP(const int sz, const int simd_width)
{
    return static_cast<int>((sz+simd_width-1)/(simd_width))*simd_width;
}

/*!
 * ************************************************************
 * *************** OS-dependent Brainfuck *********************
 * ************************************************************
 */

#ifdef _MSC_VER_
    #define FORCEINLINE __forceinline
#else
    #define FORCEINLINE __attribute__((always_inline)) inline
#endif

/*!
 *  ***********************************************************
 *  ***************** Namespace definitions *******************
 *  ***********************************************************
 */ 

#define NS_MINORANT minorant

#define NS_MINORANT_BEGIN namespace minorant {

#define NS_MINORANT_END }
