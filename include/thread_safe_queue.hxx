#pragma once

#include "defines.hxx"

template<typename T>
class thread_safe_queue
{
    public:
        std::queue<T> m_queue;
        std::mutex m_mutex;

    public:
        thread_safe_queue()
        {

        }

        thread_safe_queue(thread_safe_queue& other)
        {
            //  TODO:
        }
        
        bool empty()
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            return m_queue.empty();
        }
        
        int size()
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            return m_queue.size();
        }
        
        void enqueue(T& t)
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            m_queue.push(t);
        }
        
        bool deque(T& t)
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            if(m_queue.empty())
            {
                return false;
            }
            t=std::move(m_queue.front());
            m_queue.pop();
            return true;
        }
};
