#pragma once

#include "defines.hxx"

namespace nbsdx{
    namespace concurrent{
            /*
             *  Simple ThreadPool that creates `ThreadCount` threads upon its creation, 
             *  and pulls from a queue to get new jobs. The default is 10 threads.
             *
             *  This class requires a number of C++11 features be present in your compiler
             */ 
    };
};
