#pragma once

#include "defines.hxx"

//  is this even required
template<typename T>
class Edge
{
public:
    int m_d1;
    int m_d2;
    int m_from;
    int m_to;
    int m_idx;
    T* m_data;
    T* m_primal;
    std::mutex m_mutex;

    Edge():m_data(nullptr),m_primal(nullptr)
    {

    }

    //
    Edge(const int FROM, const int TO,const int IDX, const int D1, const int D2):
            m_d1(D1), m_d2(D2),m_idx(IDX),m_data(nullptr),m_from(FROM),m_to(TO),m_primal(nullptr)
    {

    }

    //  constructor with ptr
    Edge(T* un_ptr, const int FROM, const int TO, const int eID, const int D1, const int D2):
            m_d1(D1),m_d2(D2),m_from(FROM),m_to(TO),m_idx(eID),m_data(un_ptr),m_primal(nullptr)
    {

    }


    //  copy constructor
    Edge(const Edge<T>& other)
    {
        m_data=other.m_data;
        m_primal=other.m_primal;
        m_from=other.m_from;
        m_to=other.m_to;
        m_d2=other.m_d2;
        m_d1=other.m_d1;
        m_idx=other.m_idx;
    }

    //  move constructor
    Edge(Edge<T>&& other)
    {
        m_data=std::move(other.m_data);
        m_primal=std::move(other.m_primal);
        other.m_data=nullptr;
        other.m_primal=nullptr;
        m_from=other.m_from;
        m_to=other.m_to;
        m_d1=other.m_d1;
        m_d2=other.m_d2;
        m_idx=other.m_idx;
    }

    //  copy assignment operator
    Edge<T>& operator=(const Edge<T>& other)
    {
        Edge<T> tmp(other);
        *this=std::move(tmp);
        return *this;
    }

    //  move assignment operator
    Edge<T>& operator=(Edge<T>&& other)
    {
        delete[] m_data;
        m_data=other.m_data;
        m_primal=other.m_primal;
        other.m_data=nullptr;
        other.m_primal=nullptr;
        m_d1=other.m_d1;
        m_d2=other.m_d2;
        m_from=other.m_from;
        m_to=other.m_to;
        m_idx=other.m_idx;
        return *this;
    }

    void print()
    {
        std::cout << "EDGE INFO ";
        std::cout << "idx: " << m_idx << " ";
        std::cout << " from: " << m_from << " to: " << m_to << " (" << m_d1 << " " << m_d2 << ") \n";
    }


    ~Edge()
    {
        m_data=nullptr;
    }

};

