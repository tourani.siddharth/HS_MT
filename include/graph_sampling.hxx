#pragma once

#include "graph.hxx"
#include "defines.hxx"


//TODO: Kolmogorov's Blossom to be added here
//TODO: Edmond's Blossom implementation


auto get_hash_code(const int from, const int to, const int num_nodes)
{
    if(from>to)
    {
        return to*(num_nodes+1)*(num_nodes+1)+from*(num_nodes+1);
    }
    return from*(num_nodes+1)*(num_nodes+1)+to*(num_nodes+1);
}


/*!
 *
 * @param node_list ::  circular list of nodes the front node is the one to be sampled usually
 * @param hash_to_edge_idx :: hash code takes (from,to) of nodes to check if edge_idx is present or not
 * @param nV :: number of vertices
 * @return
 */
inline std::pair<int,int> find_next_edge(const std::vector<int>& node_list,
    std::map<int,int>& hash_to_edge_idx, const int nV)
{
    auto first_idx=node_list.front();                           //  get the first node from the list
    std::map<int,int>::iterator h2e_it;                         //  iterator defn.
    int second_idx;                                             //
    int edge_idx=-1;                                            //
    int hash_code;                                              //
    std::vector<int>::const_iterator st_it=node_list.begin();   //  iterator for the node list
    std::map<int,int>::const_iterator ste_it;                   //  not sure what for
    ++st_it;                                                    //

    int sec_it=1;
    for(;st_it!=node_list.end();++st_it)
    {
        second_idx=*st_it;                                      //  get the 2nd id
        hash_code=get_hash_code(first_idx,second_idx,nV);       //  from the first and second id, get the hash code
        ste_it=hash_to_edge_idx.find(hash_code);                //
        if(ste_it!=hash_to_edge_idx.end())
        {
            edge_idx=ste_it->second;
            break;
        }
        ++sec_it;
    }

    auto str_it=hash_to_edge_idx.find(hash_code);
    //std::cout << "*str_it: " << (*str_it).first << "\n";
    hash_to_edge_idx.erase(str_it);

    std::pair<int,int> dude(edge_idx,sec_it);
    return dude;
};


/*
 *  extract the from and the to ids from their current location in the node_list and send them to the back.
 *  depending on whether the first or second flags are active send neither or one or both.
 *
 */
inline void send_to_back(const int from, const int to,
    std::vector<int>& node_list, const bool first , const bool second)
{
    std::vector<int>::iterator it_n;

    if(first)
    {
        it_n=std::find(node_list.begin(),node_list.end(),from);
        node_list.erase(it_n);               //
        node_list.push_back(from);       //
    }

    if(second)
    {
        it_n=std::find(node_list.begin(),node_list.end(),to);
        node_list.erase(it_n);
        node_list.push_back(to);
    }
}



/*
 * go through the list of nodes present, find the location of node_id and remove it.
 * this shortens the node_list by one, if the node is found.
 */
inline void remove_node(std::vector<int>& node_list , const int node_id)
{
    //  find the node location in the list
    auto st_it=std::find(node_list.begin(),node_list.end(),node_id);
    if(st_it==node_list.end())  //
    {
        return;
    }
    else
    {
        node_list.erase(st_it); //  erase the node_list
    }
}

/*
//  REDUNDANT
inline void removeNode(std::list<int>& nodeList, int nodeID) {
    std::list<int>::iterator st_it = std::find(nodeList.begin(), nodeList.end(), nodeID);
    if (st_it == nodeList.end())
    {
        return;
    }
    nodeList.erase(st_it);  //  erase element @ this location
}
*/



template<typename T>
std::vector<int> sample_graph_1(const Graph<T>& graph)
{
    std::vector<int> s_schedule(graph.m_edges.size(),-1);

    std::cout << graph.m_edges.size() << "\n";

    std::map<int,int> node_2_num_edges_remaining;

    for(auto& nd: graph.m_nodes)
    {
        node_2_num_edges_remaining[nd.m_idx]=nd.m_nbr_edges.size();
    }
    std::map<int,int>::iterator map_it;
    std::map<int,int> hash_2_edge_id_map;
    for(auto& ed:graph.m_edges)
    {
        auto hash_code=get_hash_code(ed.m_from,ed.m_to,graph.m_nodes.size());
        hash_2_edge_id_map[hash_code]=ed.m_idx;
    }

    std::vector<bool> edges_done(graph.m_edges.size(),false);      //  This can probably be replaced by a simple counter
    int num_edges_remaining=graph.m_edges.size();

    std::vector<int> node_list;
    for(int i=0;i<graph.m_nodes.size();++i)   node_list.push_back(i);

    bool from_flag=false;
    bool to_flag=false;



    auto schedule_ID=0;
    while(num_edges_remaining>0)        //  num_edges_remaining=graph.m_edges.size();
    {
        /*       BEFORE     SCHEDULE           */
//        std::cout << "node list: ";
//        for(auto f: node_list)
//        {
//            std::cout << f << " ";
//        }
//        std::cout << "\n";
//
//        std::cout << "hash_2_edge_id_map: \n";
//        for(map_it=hash_2_edge_id_map.begin();map_it!=hash_2_edge_id_map.end();++map_it)
//        {
//            std::cout << (map_it)->first << " -> " << map_it->second  << "\n";
//        }
//
//        std::cout << "node_2_edges_remaining: \n";
//        for(map_it=node_2_num_edges_remaining.begin();map_it!=node_2_num_edges_remaining.end();++map_it)
//        {
//            std::cout << (map_it)->first << " -> " << map_it->second  << "\n";
//        }

        from_flag=true; to_flag=true;       //  from_flag is true
        auto  eID=find_next_edge(node_list,hash_2_edge_id_map, graph.m_nodes.size());   //  find the next edge
        const int from=graph.m_edges[eID.first].m_from;     //  from index
        const int to=graph.m_edges[eID.first].m_to;         //  to index
        s_schedule[schedule_ID++]=eID.first;                //  set up the s_schedule
        --num_edges_remaining;                              //  decrement the edges

        // The number of edges incident 2 node "from" is decremented by 1
        --node_2_num_edges_remaining[from];             //  subtract the nodeToNumRemaining[from]
        //  if there are no incident edges remaining, then,
        //  set it to 0.
        if(node_2_num_edges_remaining[from]==0)         //  if it is there, remove the nodetoNumRemaining[from]
        {
            remove_node(node_list,from);
            from_flag=false;
        }
        // The number of edges incident 2 node "to" is decremented by 1
        --node_2_num_edges_remaining[to];             //   subtract the nodeToNumRemaining[to]
        //  if there are no edges remaining, then,
        //  set it to 0.
        if(node_2_num_edges_remaining[to]==0)         //   if it is there, remove the nodeToNumRemaining[to]
        {
            remove_node(node_list,to);                //    remove the node "to", from the node list
            to_flag=false;                              //  set the to_flag to false
        }
        //  send this edge to the back of the list
         send_to_back(from,to,node_list,from_flag,to_flag);  //  send the nodes not removed to the back of the list

        /*               AFTER      SCHEDULE             */
//        std::cout << "node list: ";
//        for(auto f: node_list)  std::cout << f << " ";
//        std::cout << "\n";
//
//        for(map_it=hash_2_edge_id_map.begin();map_it!=hash_2_edge_id_map.end();++map_it)
//        {
//            std::cout << (map_it)->first << "-> " << map_it->second  << "\n";
//        }
//
//        std::cout << "Node Idx -> Number of Edges Incident Remaining\n";
//        std::map<int,int>::iterator map_it;
//        for(map_it=node_2_num_edges_remaining.begin();map_it!=node_2_num_edges_remaining.end();++map_it)
//        {
//            std::cout << (map_it)->first << "-> " << map_it->second  << "\n";
//        }
//        getchar();
    }


    for(int i=0;i<s_schedule.size();++i)
    {
        std::cout << "edge ID: " << s_schedule[i] << " ";
        std::cout << "From: ";
        std::cout << graph.m_edges[s_schedule[i]].m_from;
        std::cout << " -> To: ";
        std::cout << graph.m_edges[s_schedule[i]].m_to;
        std::cout << "\n";
    }
    std::cout << "\n";


    return s_schedule;
}
