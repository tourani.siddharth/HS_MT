#pragma once

#include "dGraph.hxx"
#include "graph.hxx"
#include "utils.hxx"

template<typename T>
dGraph<T> create_grid_dGraph(const int nR, const int nC, const int nL, const bool fixed_label_size)
{
    dGraph<T> graph;
    for(auto r=0;r<nR;++r)
    {
        for(auto c=0;c<nC;++c)
        {
            auto linIndex=r*nC+c;
            auto sz=nL;
            auto vec=create_random_vector<T>(sz,10);
            dNode<T> nd(linIndex,sz);
            graph.AddNode(nd,vec);
        }
    }

    auto eID=0;
    //  horizontal edges
    for(auto r=0;r<nR;++r)
    {
        for(auto c=0;c<nC-1;++c)
        {
            auto lin_index1=r*nC+c;
            auto lin_index2=r*nC+c+1;
            auto sz1=graph.m_nodes[lin_index1].m_d1;
            auto sz2=graph.m_nodes[lin_index2].m_d1;
            Edge<T> e(lin_index1,lin_index2,eID, sz1,sz2);
            auto vec=create_random_vector<T>(sz1*sz2,10);
            graph.AddEdge(e,vec);
            ++eID;
        }
    }

    //  vertical edges
    for(auto c=0;c<nC;++c)
    {
        for(auto r=0;r<nR-1;++r)
        {
            auto lin_index1=r*nC+c;
            auto lin_index2=(r+1)*nC+c;
            auto sz1=graph.m_nodes[lin_index1].m_d1;
            auto sz2=graph.m_nodes[lin_index2].m_d1;
            Edge<T> e(lin_index1,lin_index2,eID, sz1,sz2);
            auto vec=create_random_vector<T>(sz1*sz2,10);
            graph.AddEdge(e,vec);
            ++eID;
        }
    }

      graph.m_nV=graph.m_nodes.size();    //
      graph.m_nE=graph.m_edges.size();    //

    return graph;
}

template<typename T>
Graph<T> create_grid_graph(const int nR, const int nC, const int nL, const bool fixed_label_size)
{
    Graph<T> graph;
    for(auto r=0;r<nR;++r)
    {
        for(auto c=0;c<nC;++c)
        {
            auto linIndex=r*nC+c;
            auto sz=nL;
            if(!fixed_label_size)
            {
                sz=rand()%(nL-2)+2;
            }

            auto vec=create_random_vector<T>(sz,10);
            Node<T> nd(linIndex,sz);
            graph.add_node(nd,vec);
        }
    }

    auto eID=0;
    //  horizontal edges
    for(auto r=0;r<nR;++r)
    {
        for(auto c=0;c<nC-1;++c)
        {
            auto lin_index1=r*nC+c;
            auto lin_index2=r*nC+c+1;
            auto sz1=graph.m_nodes[lin_index1].m_d1;
            auto sz2=graph.m_nodes[lin_index2].m_d1;
            Edge<T> e(lin_index1,lin_index2,eID, sz1,sz2);
            auto vec=create_random_vector<T>(sz1*sz2,10);
            graph.add_edge(e,vec);
            ++eID;
        }
    }

    //  vertical edges
    for(auto c=0;c<nC;++c)
    {
        for(auto r=0;r<nR-1;++r)
        {
            auto lin_index1=r*nC+c;
            auto lin_index2=(r+1)*nC+c;
            auto sz1=graph.m_nodes[lin_index1].m_d1;
            auto sz2=graph.m_nodes[lin_index2].m_d1;
            Edge<T> e(lin_index1,lin_index2,eID, sz1,sz2);
            auto vec=create_random_vector<T>(sz1*sz2,10);
            graph.add_edge(e,vec);
            ++eID;
        }
    }

    //graph.m_nV=graph.m_nodes.size();    //
    //graph.m_nE=graph.m_edges.size();    //

    return graph;
}

template<typename T>
dGraph<T> create_complete_dGraph(const int nV, const int nL, const bool fixed_label_size)
{
    dGraph<T> graph;

    for(auto i=0;i<nV;++i)
    {
        auto linIndex=i;
        auto sz=nL;
        if(!fixed_label_size)
        {
            auto sz=rand()%(nL-2)+2;
        }

        auto vec=create_random_vector<T>(sz,10);
        dNode<T> nd(linIndex,sz);
        graph.AddNode(nd,vec);
    }

    //  completely connected graph
    auto eID=0;
    for(auto i=0;i<nV;++i)
    {
        for(auto j=i+1;j<nV;++j)
        {
            auto sz1=graph.m_nodes[i].m_d1;
            auto sz2=graph.m_nodes[j].m_d1;
            Edge<T> e(i,j,eID, sz1,sz2);
            auto vec=create_random_vector<T>(sz1*sz2,10);
            graph.AddEdge(e,vec);
            ++eID;
        }
    }

    return graph;
}

template<typename T>
Graph<T> create_complete_graph(const int nV, const int nL, const bool fixed_label_size)
{
    Graph<T> graph;
    for(auto i=0;i<nV;++i)
    {
        auto linIndex=i;
        auto sz=nL;
        if(!fixed_label_size)
        {
            auto sz=rand()%(nL-2)+2;
        }

        auto vec=create_random_vector<T>(sz,10);
        Node<T> nd(linIndex,sz);
        graph.add_node(nd,vec);
    }

    //  completely connected graph
    auto eID=0;
    for(auto i=0;i<nV;++i)
    {
        for(auto j=i+1;j<nV;++j)
        {
            auto sz1=graph.m_nodes[i].m_d1;
            auto sz2=graph.m_nodes[j].m_d1;
            Edge<T> e(i,j,eID, sz1,sz2);
            auto vec=create_random_vector<T>(sz1*sz2,10);
            graph.add_edge(e,vec);
            ++eID;
        }
    }



    graph.m_nV=graph.m_nodes.size();    //  number of nodes
    graph.m_nE=graph.m_edges.size();    //  number of edges

    return graph;
}
