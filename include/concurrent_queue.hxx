#pragma once

#include "defines.hxx"


/*   */
template<typename T>
class concurrent_queue
{
    public:
        concurrent_queue()=default;
        concurrent_queue(const concurrent_queue<T>&)=delete;        //  Compiler does not construct this
        concurrent_queue(const concurrent_queue<T>&&)=delete;       //  Compiler does not construct this

        T pop()
        {
            std::unique_lock<std::mutex> mlock(mutex_);
            while(queue_.empty())
            {
                cond_.wait(mlock);
            }
            T item=queue_.front();
            queue_.pop();
            return item;
        }

        void pop(T& item)
        {
            std::unique_lock<std::mutex> mlock(mutex_);
            while(queue_.empty())
            {
                cond_.wait(mlock);
            }
            item=queue_.front();
            queue_.pop();
        }

        void push(const T& item)
        {
            std::unique_lock<std::mutex> mlock(mutex_);
            queue_.push(item);
            mlock.unlock();
            cond_.notify_one();
        }

        void push_vector(const std::vector<T> vec)
        {
            std::unique_lock<std::mutex> mlock(mutex_);
            for(int i=0;i<vec.size();++i)
            {
                queue_.push(vec[i]);
            }
            mlock.unlock();
            cond_.notify_one();
        }
        
        void push(T&& item)
        {
            std::unique_lock<std::mutex> mlock(mutex_);
            queue_.push(std::move(item));
            mlock.unlock();
            cond_.notify_one();
        }

        bool is_empty()
        {
            if(queue_.empty())
            {
                return true;
            }
            return false;
        }

    private:
        std::queue<T> queue_;
        std::mutex mutex_;
        std::condition_variable cond_;
};
